#include "file_bmp.h"

#define RGB_BYTE 3
#define BMP_ALIGN 4

#define BF_TYPE 0x4d42
#define BIT_COUNT 24
#define PLANES 1
#define COMPRESSION 0
#define FILE_SIZE 0
#define SIZE 40
#define SIZE_IMAGE 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

int read_header(FILE *in, uint64_t *width, uint64_t *height)
{
    struct bmp_header bmp_head;
    if (fread(&bmp_head, sizeof(struct bmp_header), 1, in) != 1) return 1;

    *width = bmp_head.biWidth;
    *height = bmp_head.biHeight;

    if (bmp_head.bfType != BF_TYPE) return 1;
    if (bmp_head.biBitCount != BIT_COUNT) return 1;
    if (bmp_head.biPlanes != PLANES) return 1;
    if (bmp_head.biCompression != COMPRESSION) return 1;
    if (fseek(in, bmp_head.bOffBits, SEEK_SET) != 0) return 1;
    
    return 0;
}

enum status from_bmp(FILE *in, struct image *img)
{
    uint64_t width;
    uint64_t height;

    int flag = read_header(in, &width, &height);

    if (flag) return READ_HEADER_PROBLEM;
    
    if (create_image(img, width, height) == MEMORY_LACK){
        return MEMORY_LACK;
    }    

    uint16_t padding = find_padding(img->width);

    struct pixel *data = img->data;
    uint32_t reading = img->width * 3 + padding;

    for (uint32_t i = 0; i < img->height; i++){
        if (fread(data, 1, reading, in) - reading != 0){
            return READ_BMP_PROBLEM;
        }
        else data += img->width;
    }

    return OK;
}

enum status to_bmp(FILE *out, struct image *img)
{
    struct bmp_header bmp_head = {0};

    uint16_t padding = find_padding(img->width);

    uint64_t width = img->width;
    uint64_t height = img->height;
    

    bmp_head.bfType = BF_TYPE;
    bmp_head.bfileSize = sizeof(struct bmp_header) + (width * 3 + padding) * height;
    bmp_head.bfileSize = FILE_SIZE;
    bmp_head.bOffBits = sizeof(struct bmp_header);
    bmp_head.biSize = SIZE;
    bmp_head.biWidth = width;
    bmp_head.biHeight = height;
    bmp_head.biPlanes = PLANES;
    bmp_head.biBitCount = BIT_COUNT;
    bmp_head.biCompression = COMPRESSION;
    bmp_head.biSizeImage = SIZE_IMAGE;
    bmp_head.biXPelsPerMeter = X_PELS_PER_METER;
    bmp_head.biYPelsPerMeter = Y_PELS_PER_METER;
    bmp_head.biClrUsed = CLR_USED;
    bmp_head.biClrImportant = CLR_IMPORTANT;


    if (fwrite(&bmp_head, sizeof(struct bmp_header), 1, out) != 1) return WRITE_PROBLEM;

    void *fix = img->data;

    uint64_t writing = width * 3 + padding;
    for (uint64_t j = 0; j < height; j++){
        if (fwrite(img->data, 1, writing, out) - writing != 0) return WRITE_PROBLEM;
        else img->data += width;
    }

    img->data = fix;

    return OK;
}

uint16_t find_padding(uint64_t width){
    uint16_t padding = BMP_ALIGN - (width * RGB_BYTE) % BMP_ALIGN;
    if (padding == BMP_ALIGN) {
        padding = 0;
    }
    return padding;
}
