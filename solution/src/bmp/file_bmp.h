#ifndef FILE_BMP_H
#define FILE_BMP_H

#include "../img/image.h"
#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>



enum status from_bmp( FILE* in, struct image* img);

enum status to_bmp( FILE* out, struct image* img);

int read_header(FILE *in, uint64_t *width, uint64_t *height);

uint16_t find_padding(uint64_t width);


#endif


