#include "file_handler.h"
#include <errno.h>
#include <string.h>

int read(char *file_path, struct image *img){
    printf("Opening file to read \n");
    FILE *file = fopen(file_path, "rb");
    if (file == 0){
        fprintf(stderr, "%s \n", file_path);
        fprintf(stderr, "%s", strerror(errno));
        fprintf(stderr, "cannot open file to read \n");
        return 1;
    }
    enum status flag  = from_bmp(file, img);
    fclose(file);
    if (flag == OK){
        printf("img successfully read \n");
        return 0;
    }
    if (flag == MEMORY_LACK){
        fprintf(stderr, "not enough memory to allocate for the picture \n");
        return 1;
    }
    if (flag == READ_HEADER_PROBLEM){
        fprintf(stderr, "header of bmp is incorrect \n");
        return 1;
    }
    if (flag == READ_BMP_PROBLEM){
        fprintf(stderr, "bmp file is incorrect \n");
        return 1;
    }
    return 1;

}

int write(char *file_path, struct image *img){
    printf("Opening file to write \n");
    FILE *file = fopen(file_path, "wb");
    if (file == 0){
        fprintf(stderr, "cannot open file to write \n");
        return 1;
    }
    enum status flag = to_bmp(file, img);
    fclose(file);
    if (flag == OK){
        printf("img successfully writen to file \n");
        return 0;
    }
    if (flag == WRITE_PROBLEM){
        fprintf(stderr, "can not write to whis file \n");
        return 1;
    }
    return 1;
}
