#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H

#include "file_bmp.h"

#include <stdio.h>


int read(char *file_path, struct image *img);

int write(char *file_path, struct image *img);

#endif
