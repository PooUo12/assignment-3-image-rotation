#include "image.h"

enum status create_image(struct image *img,  uint64_t width, uint64_t height){
    
    img->width = width;
    img->height = height;

    size_t len = width* height * (sizeof(struct pixel))+4;
    img -> data = malloc(len);

    if (img->data == 0){
        return MEMORY_LACK;
    }

    for (size_t i = 0; i < len; i++){
        ((char*)img->data)[i] = 0;
    }

    return OK;
}

struct pixel get_pixel(struct image *img,  uint64_t width, uint64_t height){
    return img->data[img->width * height + width];
}

void set_pixel(struct image *img, uint64_t width, uint64_t height, struct pixel pixel){
    img->data[img->width* height + width] = pixel;
}

void clear_image(struct image *img){
    img -> width = 0;
    img -> height = 0;
    free(img -> data);
}
