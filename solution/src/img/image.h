#ifndef IMAGE_H
#define IMAGE_H


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { 
    uint8_t b, g, r; 
};

enum status {
    OK,
    MEMORY_LACK,
    WRITE_PROBLEM,
    READ_BMP_PROBLEM,
    READ_HEADER_PROBLEM
};

enum status create_image(struct image* img, uint64_t width, uint64_t height);

struct pixel get_pixel(struct image* img, uint64_t width, uint64_t height);

void set_pixel(struct image* img, uint64_t width, uint64_t height, struct pixel pixel);

void clear_image(struct image* img);

#endif
