#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

int rotate(struct image *img);

#endif

