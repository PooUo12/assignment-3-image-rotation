#include "bmp/file_handler.h"
#include "img/rotate.h"

#include <stdio.h>


int main( int argc, char** argv ) {

    if (argc < 3){
        printf("not enough arguments");
        return 1;
    }

    struct image img = {0};

    if(read(argv[1], &img)){
        clear_image(&img);
        return 1;
    }

    if (rotate(&img)){
        clear_image(&img);
        return 1;
    }

    if (write(argv[2], &img)){
        clear_image(&img);
        return 1;
    }

    clear_image(&img);

    return 0;
}
